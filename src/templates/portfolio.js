import React, { useRef, useEffect } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { GatsbyImage } from 'gatsby-plugin-image'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Layout from '../components/layout'
import BackgroundAngle from '../components/backgroundAngle'
import SectionTitle from '../components/sectionTitle'

const Portfolio = ({ pageContext }) => {
  const topRef = useRef(null)

  const data = useStaticQuery(graphql`
    query PortfolioQuery {
      allDatoCmsPortfolio {
        edges {
          node {
            slug
            pageTitle
            category
            headerImage {
              gatsbyImageData(layout: FULL_WIDTH)
            }
            seoMetaTags {
              ...GatsbyDatoCmsSeoMetaTags
            }
            body {
              textNode {
                childMarkdownRemark {
                  html
                }
              }
              image {
                gatsbyImageData(layout: FULL_WIDTH)
                alt
              }
            }
          }
        }
      }
    }
  `)

  // Find the correct portfolio data based on the slug
  const portfolio = data.allDatoCmsPortfolio.edges.find(
    ({ node }) => node.slug === pageContext.slug
  )?.node

  useEffect(() => {
    if (topRef.current) {
      topRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }, [pageContext.slug])

  if (!portfolio) return null // or some error component

  return (
    <Layout
      headerDetails={{
        image: portfolio.headerImage,
        title: 'Projects',
        subTitle:
          'Ini adalah beberapa proyek dari Kami. Lihat semuanya untuk mengetahui gaya dan keterampilan kami.',
        fullHeight: false,
      }}
    >
      <div ref={topRef}></div>
      <HelmetDatoCms seo={portfolio.seoMetaTags} />
      <div className="section">
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper ">
          <SectionTitle
            category={portfolio.category}
            title={portfolio.pageTitle}
            color="blue"
          />
          {portfolio.body.map((record, i) => {
            return (
              <div key={i} className="bottom-margin-20">
                <div
                  dangerouslySetInnerHTML={{
                    __html: record.textNode.childMarkdownRemark.html,
                  }}
                />
                {record.image && record.image.gatsbyImageData && (
                  <GatsbyImage
                    image={record.image.gatsbyImageData}
                    alt={record.image.alt || ''}
                    className="top-margin-40"
                    objectFit="cover"
                    style={{ boxShadow: '2px 4px 10px rgba(51, 51, 51, 0.3)' }}
                  />
                )}
              </div>
            )
          })}
        </div>
      </div>
    </Layout>
  )
}

export default Portfolio
