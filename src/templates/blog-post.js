import React, { useRef, useEffect } from 'react'
import { graphql, Link } from 'gatsby'
import { GatsbyImage } from 'gatsby-plugin-image'
import AdSense from '../components/adsense'
import parse, { domToReact } from 'html-react-parser'
import Layout from '../components/layout'
import BackgroundAngle from '../components/backgroundAngle'
import SectionTitle from '../components/sectionTitle'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { defineCustomElements as deckDeckGoHighlightElement } from '@deckdeckgo/highlight-code/dist/loader'

// Call this once in your app
deckDeckGoHighlightElement()

const BlogPostTemplate = ({ data, location }) => {
  const topRef = useRef(null)

  const post = data.post
  const allPosts = data.allPosts.nodes

  // Find the index of the current post
  const currentIndex = allPosts.findIndex((p) => p.slug === post.slug)

  // Determine the newer and older posts
  const newerPost = currentIndex > 0 ? allPosts[currentIndex - 1] : null
  const olderPost =
    currentIndex < allPosts.length - 1 ? allPosts[currentIndex + 1] : null

  const insertAdPlaceholder = (html) => {
    let paragraphCount = 0
    let adCount = 0
    const totalParagraphs = (html.match(/<p/g) || []).length
    const adPositions = [1, Math.floor(totalParagraphs / 2), totalParagraphs]

    const options = {
      replace: (domNode) => {
        if (domNode.type === 'tag' && domNode.name === 'table') {
          const tableWithHeaders = addHeadersToTableCells(domNode)
          return (
            <div className="table-responsive">
              <table className="blog-table">
                {domToReact(tableWithHeaders.children, options)}
              </table>
            </div>
          )
        }
        if (domNode.type === 'tag' && domNode.name === 'p') {
          paragraphCount++

          if (adPositions.includes(paragraphCount) && adCount < 3) {
            adCount++
            return (
              <React.Fragment>
                <p>{domToReact(domNode.children, options)}</p>
                <div className="ad-container" style={{ margin: '20px 0' }}>
                  <AdSense currentPath={location.pathname} />
                </div>
              </React.Fragment>
            )
          }
        }
        if (domNode.type === 'tag' && domNode.name === 'pre') {
          // This will ensure the code blocks are properly highlighted
          return (
            <pre
              dangerouslySetInnerHTML={{ __html: domNode.children[0].data }}
            />
          )
        }
      },
    }

    return parse(html, options)
  }

  const addHeadersToTableCells = (tableNode) => {
    let headers = []

    // Function to safely extract text from a node
    const getNodeText = (node) => {
      if (node.children && node.children[0] && node.children[0].data) {
        return node.children[0].data.trim()
      }
      return ''
    }

    // Find headers
    const findHeaders = (node) => {
      if (node.name === 'th') {
        headers.push(getNodeText(node))
      } else if (node.children) {
        node.children.forEach(findHeaders)
      }
    }

    // Find the first row if no headers are found
    const findFirstRowCells = (node) => {
      if (node.name === 'tr') {
        node.children.forEach((cell) => {
          headers.push(getNodeText(cell))
        })
        return true // Stop after first row
      } else if (node.children) {
        return node.children.some(findFirstRowCells)
      }
      return false
    }

    // Try to find headers
    findHeaders(tableNode)

    // If no headers found, use the first row
    if (headers.length === 0) {
      findFirstRowCells(tableNode)
    }

    // We don't need to add data-label attributes anymore
    // as we'll keep the thead visible

    return tableNode
  }

  useEffect(() => {
    if (topRef.current) {
      topRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }, [post.slug])

  if (!post) {
    return <div>Loading...</div> // Or any other loading/error state
  }

  return (
    <Layout
      headerDetails={{
        image: post.headerImage,
        title: 'Articles',
        subTitle:
          'Kami telah membangun website selama lebih dari 5 tahun ingin berbagi pengalaman.',
        fullHeight: false,
      }}
    >
      <div ref={topRef}></div>
      <HelmetDatoCms seo={post.seoMetaTags} />
      <div className="section">
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper">
          <SectionTitle category="Writing" title={post.title} color="blue" />
          {post.body.map((record, i) => {
            if (record.paragraphNode) {
              const contentWithAds = insertAdPlaceholder(
                record.paragraphNode.childMarkdownRemark.html
              )
              return <React.Fragment key={i}>{contentWithAds}</React.Fragment>
            } else if (record.source && record.source.gatsbyImageData) {
              return (
                <div key={i} className="bottom-margin-20">
                  <GatsbyImage
                    image={record.source.gatsbyImageData}
                    className="top-margin-20"
                    objectFit="cover"
                    style={{ boxShadow: '2px 4px 10px rgba(51, 51, 51, 0.3)' }}
                  />
                </div>
              )
            }
            return null // Handle case where neither paragraphNode nor source exists
          })}
          <div className="post-navigation">
            {newerPost && (
              <Link
                to={`/blog/${newerPost.slug}`}
                className="post-navigation__button post-navigation__button--prev"
              >
                <span className="post-navigation__direction">← Newer Post</span>
                <span className="post-navigation__title">
                  {newerPost.title}
                </span>
              </Link>
            )}
            {olderPost && (
              <Link
                to={`/blog/${olderPost.slug}`}
                className="post-navigation__button post-navigation__button--next"
              >
                <span className="post-navigation__direction">Older Post →</span>
                <span className="post-navigation__title">
                  {olderPost.title}
                </span>
              </Link>
            )}
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default BlogPostTemplate

export const query = graphql`
  query postQuery($slug: String!) {
    post: datoCmsBlogPost(slug: { eq: $slug }) {
      title
      slug
      headerImage {
        gatsbyImageData(layout: FULL_WIDTH)
      }
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      body {
        ... on DatoCmsImage {
          id
          source {
            gatsbyImageData(layout: FULL_WIDTH, width: 1000)
          }
        }
        ... on DatoCmsText {
          id
          paragraphNode {
            childMarkdownRemark {
              html
            }
          }
        }
      }
    }
    allPosts: allDatoCmsBlogPost(sort: { meta: { createdAt: DESC } }) {
      nodes {
        title
        slug
        meta {
          createdAt
        }
      }
    }
  }
`
