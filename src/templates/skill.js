import React, { useState, useEffect, useRef } from 'react'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { GatsbyImage } from 'gatsby-plugin-image'
import { graphql, Link } from 'gatsby'
import Layout from '../components/layout'
import BackgroundAngle from '../components/backgroundAngle'
import SectionTitle from '../components/sectionTitle'
import ContactBanner from '../components/contactBanner'
import MarketingCard from '../components/marketingCard'

const Pagination = ({ currentPage, totalPages, paginate }) => {
  const pageNumbers = []
  const maxVisiblePages = 3

  let startPage = Math.max(1, currentPage - Math.floor(maxVisiblePages / 2))
  let endPage = Math.min(totalPages, startPage + maxVisiblePages - 1)

  if (endPage - startPage + 1 < maxVisiblePages) {
    startPage = Math.max(1, endPage - maxVisiblePages + 1)
  }

  for (let i = startPage; i <= endPage; i++) {
    pageNumbers.push(i)
  }

  return (
    <div className="pagination">
      <button
        onClick={() => paginate(currentPage - 1)}
        disabled={currentPage === 1}
        className="pagination__button pagination__button--prev"
      >
        Prev
      </button>
      {pageNumbers.map((number) => (
        <button
          key={number}
          onClick={() => paginate(number)}
          className={`pagination__button ${currentPage === number ? 'pagination__button--active' : ''}`}
        >
          {number}
        </button>
      ))}
      <button
        onClick={() => paginate(currentPage + 1)}
        disabled={currentPage === totalPages}
        className="pagination__button pagination__button--next"
      >
        Next
      </button>
    </div>
  )
}

const SkillTemplate = ({ data }) => {
  const skill = data.skill
  const homeSection = data.home.pageSections.find(
    (section) => section.category.toLowerCase() === skill.category.toLowerCase()
  )

  const topRef = useRef(null)

  // Sort the body content by createdAt in descending order
  const sortedBody = [...skill.body].sort(
    (a, b) => new Date(b.meta.createdAt) - new Date(a.meta.createdAt)
  )

  const [currentPage, setCurrentPage] = useState(1)
  const postsPerPage = 4
  const indexOfLastPost = currentPage * postsPerPage
  const indexOfFirstPost = indexOfLastPost - postsPerPage
  const currentPosts = sortedBody.slice(indexOfFirstPost, indexOfLastPost)
  const totalPages = Math.ceil(sortedBody.length / postsPerPage)

  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber)
  }

  useEffect(() => {
    if (topRef.current) {
      topRef.current.scrollIntoView({ behavior: 'smooth' })
    }
  }, [currentPage])

  return (
    <Layout
      headerDetails={{
        image: skill.headerImage,
        title: skill.title,
        subTitle: skill.subTitle,
        fullHeight: false,
      }}
    >
      <div ref={topRef}></div>
      <HelmetDatoCms seo={skill.seoMetaTags} />
      <div className="section">
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper ">
          <SectionTitle
            category={skill.category}
            title={skill.pageTitle}
            color="blue"
          />
          {skill.description && (
            <h5
              className="bottom-margin-40"
              dangerouslySetInnerHTML={{ __html: skill.description }}
            />
          )}
          {currentPosts.map((section, index) => {
            if (section.image) {
              return (
                <div className="flex-grid bottom-margin-40" key={index}>
                  <div
                    className={`flex1 picture ${section.imageLeft ? 'order-1' : 'order-2'}`}
                  >
                    <div className="image-container">
                      {section.image && section.image.gatsbyImageData && (
                        <GatsbyImage
                          image={section.image.gatsbyImageData}
                          alt={section.image.alt || ''}
                          style={{ height: '100%', width: '100%' }}
                          objectFit="cover"
                        />
                      )}
                    </div>
                  </div>
                  <div
                    className={`flex4 ${section.imageLeft ? 'order-2' : 'order-1'}`}
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html: section.textNode.childMarkdownRemark.html,
                      }}
                    />
                    {section.pageLink && (
                      <div style={{ display: 'flex', marginTop: '15px' }}>
                        <Link
                          to={`/${section.pageLink}`}
                          className="button outline-blue"
                        >
                          Read More
                        </Link>
                      </div>
                    )}
                  </div>
                </div>
              )
            } else if (section.homePageSection) {
              return (
                <>
                  <div className="flex-grid bottom-margin-40">
                    <div className="flex1">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: section.textNode.childMarkdownRemark.html,
                        }}
                      />
                    </div>
                  </div>
                  <div className="flex-grid bottom-margin-40">
                    <div className="flex1">
                      {[1, 2, 3, 4].map((num) => {
                        return (
                          <MarketingCard
                            key={num}
                            service={homeSection[`marketingCard${num}`]}
                          />
                        )
                      })}
                    </div>
                  </div>
                </>
              )
            } else if (!section.image) {
              return (
                <div className="flex-grid bottom-margin-40">
                  <div className="flex1">
                    <div
                      dangerouslySetInnerHTML={{
                        __html: section.textNode.childMarkdownRemark.html,
                      }}
                    />
                  </div>
                </div>
              )
            }
            return <div>There's missing content here.</div>
          })}
          {sortedBody.length > 3 && (
            <Pagination
              currentPage={currentPage}
              totalPages={totalPages}
              paginate={paginate}
            />
          )}
        </div>
        <ContactBanner />
      </div>
    </Layout>
  )
}

export default SkillTemplate

export const query = graphql`
  query skillQuery($slug: String!) {
    skill: datoCmsSkill(slug: { eq: $slug }) {
      slug
      headerImage {
        gatsbyImageData(layout: FULL_WIDTH)
      }
      title
      subTitle
      category
      pageTitle
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      body {
        pageLink
        imageCenter
        imageLeft
        imageRight
        homePageSection
        textNode {
          childMarkdownRemark {
            html
          }
        }
        image {
          gatsbyImageData(layout: FULL_WIDTH)
        }
        meta {
          createdAt
        }
      }
    }
    home: datoCmsIndexPage {
      pageSections {
        marketingCard4
        marketingCard3
        marketingCard1
        marketingCard2
        category
        slug
      }
    }
  }
`
