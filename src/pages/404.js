import React from 'react'
import { graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Layout from '../components/layout'
import SectionTitle from '../components/sectionTitle'
import ContactForm from '../components/contactForm'
import BackgroundAngle from '../components/backgroundAngle'

const NotFoundPage = ({ data }) => (
  <Layout
    headerDetails={{
      image: data.datoCmsContactPage.headerImage,
      title: 'Page Not Found',
      subTitle:
        'Apa yang kamu cari tidak ada di sini. Coba gunakan URL yang berbeda.',
      fullHeight: false,
    }}
  >
    <HelmetDatoCms />
    <div className="section">
      <BackgroundAngle color="255,255,255" slash="forward" />
      <div className="page-wrapper ">
        <SectionTitle
          category="Failure"
          title="Kami tidak dapat menemukan halaman yang di cari"
          color="blue"
        />
        <div className="flex-grid">
          <div className="flex4">
            <div>
              <h4>Sayangnya halaman tidak dapat ditemukan</h4>
              <p>
                Website salalu berubah begitu pula alamat URL; terlepas itu Kami
                sangat menyesal bahwa halaman tidak ada di sini. Mungkin mencoba
                menavigasi ke halaman lain.
              </p>
              <br />
              <p>
                Kamu juga dapat mengisi formulir yang ada di sini. Ajukan
                pertanyaan apa pun yang kamu miliki tentang marketing atau web
                design dan sebagainya.
              </p>
            </div>
          </div>
          <div className="flex1">
            <ContactForm showHeader={true} background={true} />
          </div>
        </div>
      </div>
    </div>
  </Layout>
)

export default NotFoundPage

export const query = graphql`
  query NotFoundPage {
    datoCmsContactPage {
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      headerImage {
        gatsbyImageData(layout: FULL_WIDTH)
      }
    }
  }
`
