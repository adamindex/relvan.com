import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import PageSection from '../components/pageSection'

const IndexPage = ({ data }) => (
  <Layout
    headerDetails={{
      image: data.datoCmsIndexPage.headerImage,
      title: data.datoCmsIndexPage.pageTitle,
      subTitle:
        data.datoCmsIndexPage.subTitleNode.childMarkdownRemark.rawMarkdownBody,
      fullHeight: true,
    }}
  >
    {data.datoCmsIndexPage.pageSections.map((section, i) => {
      let loweredCat = section.category.toLowerCase()
      return (
        <div
          key={i}
          className={`section ${loweredCat} ${loweredCat === 'services' || loweredCat === 'customers' ? 'blue' : ''}`}
        >
          <PageSection data={section} />
        </div>
      )
    })}
  </Layout>
)

export default IndexPage

export const query = graphql`
  query IndexQuery {
    datoCmsIndexPage {
      pageTitle
      headerImage {
        gatsbyImageData(layout: FULL_WIDTH)
      }
      subTitleNode {
        childMarkdownRemark {
          rawMarkdownBody
        }
      }
      pageSections {
        category
        htmlNode {
          childMarkdownRemark {
            html
            rawMarkdownBody
          }
        }
        image {
          gatsbyImageData(layout: FULL_WIDTH)
        }
        imageGallery {
          gatsbyImageData(height: 120, layout: FIXED)
        }
        marketingCard1
        marketingCard2
        marketingCard3
        marketingCard4
        servicesCard1
        servicesCard2
        servicesCard3
        servicesCard4
        title
      }
    }
  }
`
