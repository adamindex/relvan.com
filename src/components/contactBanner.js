import React from 'react'
import { Link } from 'gatsby'

const ContactBanner = () => {
  return (
    <div className="banner orange">
      <div className="page-wrapper">
        <div className="banner__inner centered">
          <h3 className="title">Ada Pertanyaan?</h3>
          <Link to="/contact" className="button outline-black">
            Get in touch
          </Link>
        </div>
      </div>
    </div>
  )
}

export default ContactBanner
