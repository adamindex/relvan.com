import React, { useEffect, useState } from 'react'

const AdSense = ({ currentPath }) => {
  const [isScriptLoaded, setIsScriptLoaded] = useState(false)
  const [isClientSide, setIsClientSide] = useState(false)

  useEffect(() => {
    setIsClientSide(true)
    if (process.env.NODE_ENV === 'production') {
      const script = document.createElement('script')
      script.src =
        'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-1000224758595089'
      script.async = true
      script.crossOrigin = 'anonymous'
      script.onload = () => setIsScriptLoaded(true)
      document.head.appendChild(script)
    }
  }, [])

  useEffect(() => {
    if (isScriptLoaded && process.env.NODE_ENV === 'production') {
      try {
        ;(window.adsbygoogle = window.adsbygoogle || []).push({})
      } catch (error) {
        console.error('AdSense error:', error)
      }
    }
  }, [isScriptLoaded, currentPath])

  if (!isClientSide) {
    return null // Return null on server-side rendering
  }

  if (process.env.NODE_ENV === 'development') {
    return (
      <div
        style={{
          background: '#02000091',
          width: '728px',
          height: '90px',
          color: '#fff',
          textAlign: 'center',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        Ad Placeholder (Development Mode)
      </div>
    )
  }

  return (
    <div
      style={{
        width: '728px',
        height: '90px',
        background: '#f0f0f0',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {!isScriptLoaded ? (
        'Loading Ad...'
      ) : (
        <ins
          className="adsbygoogle"
          style={{ display: 'block', width: '100%', height: '100%' }}
          data-ad-client="ca-pub-1000224758595089"
          data-ad-slot="8805190426"
          data-ad-format="auto"
          data-full-width-responsive="true"
        />
      )}
    </div>
  )
}

export default AdSense
