import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faCrosshairs,
  faPencilRuler,
  faChartLine,
  faSitemap,
  faGlobeAmericas,
  faCommentDollar,
} from '@fortawesome/free-solid-svg-icons'

const ServiceCard = ({ service }) => {
  // Parse the service data if it's a string
  const serviceData =
    typeof service === 'string' ? JSON.parse(service) : service

  const setIcon = (iconName) => {
    switch (iconName) {
      case 'Crosshairs':
        return <FontAwesomeIcon icon={faCrosshairs} />
      case 'PencilRuler':
        return <FontAwesomeIcon icon={faPencilRuler} />
      case 'ChartLine':
        return <FontAwesomeIcon icon={faChartLine} />
      case 'Sitemap':
        return <FontAwesomeIcon icon={faSitemap} />
      case 'MoneyBubble':
        return <FontAwesomeIcon icon={faCommentDollar} />
      default:
        return <FontAwesomeIcon icon={faGlobeAmericas} />
    }
  }

  return (
    <div className="service-card">
      <div className="icon">{setIcon(serviceData.icon)}</div>
      <h3 className="title">{serviceData.title}</h3>
      <div className="text">{serviceData.text}</div>
    </div>
  )
}

export default ServiceCard
