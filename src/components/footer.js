import React from 'react'
import { Link } from 'gatsby'

const Footer = ({ social, navItems }) => {
  return (
    <footer className="footer">
      <div className="page-wrapper">
        <div className="flex-grid">
          <div className="flex1">
            <h4>Digital Marketing</h4>
            {/* <p><Link to="about">About Me</Link></p> */}
            <p>
              <Link to="/">Resume</Link>
            </p>
            {/* <p><Link to="">Technology Blog</Link></p> */}
          </div>
          <div className="flex1">
            <h4>Our Services</h4>
            {navItems.map((item, i) => (
              <p key={i}>
                <Link to={`/${item.node.slug}`}>{item.node.title}</Link>
              </p>
            ))}
          </div>
          <div className="flex1">
            <h4>Free Consultation</h4>
            <p>
              <Link to="/contact">Get in touch</Link>
            </p>
            {social.edges.map((profile, i) => {
              return (
                <p key={i}>
                  <a href={profile.node.url}>{profile.node.display}</a>
                </p>
              )
            })}
          </div>
        </div>
        <div className="text-right copywrite">
          <p>
            &copy;{new Date().getFullYear().toString()} Relvan - All Rights
            Reserved.
          </p>
        </div>
      </div>
    </footer>
  )
}

export default Footer
