import React from 'react'
import { graphql, useStaticQuery, Link } from 'gatsby'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'

const BlogCarousel = () => {
  const data = useStaticQuery(graphql`
    query BlogCarousel {
      allDatoCmsBlogPost(limit: 3, sort: { meta: { createdAt: DESC } }) {
        edges {
          node {
            title
            slug
            featuredImage {
              gatsbyImageData(layout: CONSTRAINED)
            }
            snippitNode {
              childMarkdownRemark {
                html
              }
            }
            meta {
              createdAt(formatString: "MMMM DD, YYYY")
            }
          }
        }
      }
    }
  `)
  const posts = data.allDatoCmsBlogPost.edges
  return (
    <div className="flex-grid top-margin-40">
      {posts.map(({ node: post }) => {
        const image = getImage(post.featuredImage) || post.featuredImage
        return (
          <div className="flex1" key={post.slug}>
            <Link to={`/blog/${post.slug}`}>
              <div className="card">
                <div className="card__image">
                  {image && (
                    <GatsbyImage
                      image={image}
                      alt={post.title || 'Blog post featured image'}
                    />
                  )}
                </div>
                <div className="card__caption">
                  <div className="card__title">{post.title}</div>
                  <div className="card__date">{post.meta.createdAt}</div>
                  <div
                    className="card__description text-clip"
                    dangerouslySetInnerHTML={{
                      __html: post.snippitNode.childMarkdownRemark.html,
                    }}
                  />
                </div>
              </div>
            </Link>
          </div>
        )
      })}
    </div>
  )
}

export default BlogCarousel
