import React from 'react'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import SectionTitle from './sectionTitle'
import ServiceCard from './serviceCard'
import BlogCarousel from './blogCarousel'
import ContactForm from './contactForm'
import MarketingCard from './marketingCard'
import BackgroundAngle from './backgroundAngle'

const PageSection = ({ data }) => {
  const sectionType = data.category
  const image = getImage(data.image) || data.image

  if (sectionType === 'Hero') {
    return (
      <>
        {image && (
          <GatsbyImage
            image={image}
            alt="Background image"
            className="background-image"
            style={{ position: 'absolute' }}
          />
        )}
        <div className="page-wrapper text-center">
          <SectionTitle
            category={data.category}
            title={data.title}
            color="white"
          />
          <div
            className="body"
            dangerouslySetInnerHTML={{
              __html: data.htmlNode.childMarkdownRemark.html,
            }}
          />
        </div>
      </>
    )
  } else if (sectionType === 'Discover') {
    return (
      <>
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper">
          <div className="flex-grid">
            <div className="flex1 picture">
              {image && (
                <GatsbyImage
                  image={image}
                  alt={data.alt || 'Section image'}
                  objectFit="cover"
                />
              )}
            </div>
            <div className="flex3">
              <SectionTitle
                category={data.category}
                title={data.title}
                color="blue"
              />
              <div
                className="body"
                dangerouslySetInnerHTML={{
                  __html: data.htmlNode.childMarkdownRemark.html,
                }}
              />
            </div>
          </div>
        </div>
      </>
    )
  } else if (sectionType === 'Services') {
    return (
      <div className="page-wrapper">
        <SectionTitle
          category={data.category}
          title={data.title}
          color="white"
        />
        <div className="flex-grid">
          {[1, 2, 3, 4].map((num) => {
            return (
              <div key={num} className="flex1 two-wide">
                <ServiceCard service={data[`servicesCard${num}`]} />
              </div>
            )
          })}
        </div>
      </div>
    )
  } else if (sectionType === 'Writing') {
    return (
      <>
        <BackgroundAngle color="187, 149, 60" slash="back" />
        <div className="page-wrapper">
          <SectionTitle
            category={data.category}
            title={data.title}
            color="black"
            catColor="blue"
            link="blog"
          />
          <div
            className="body"
            dangerouslySetInnerHTML={{
              __html: data.htmlNode.childMarkdownRemark.html,
            }}
          />
          <BlogCarousel count={3} />
        </div>
      </>
    )
  } else if (sectionType === 'Contact') {
    return (
      <>
        {image && (
          <GatsbyImage
            image={image}
            alt="Background image"
            className="background-image"
            style={{ position: 'absolute' }}
          />
        )}
        <div className="page-wrapper text-center">
          <SectionTitle
            category={data.category}
            title={data.title}
            color="white"
            link="contact"
          />
          <div
            className="body text-white"
            dangerouslySetInnerHTML={{
              __html: data.htmlNode.childMarkdownRemark.html,
            }}
          />
          <div className="flex-grid">
            <div className="flex1 mobile-hidden"></div>
            <div className="flex1">
              <ContactForm />
            </div>
            <div className="flex1 mobile-hidden"></div>
          </div>
        </div>
      </>
    )
  } else if (sectionType === 'Software') {
    const smallImage = getImage(data.smallImage) || data.smallImage
    return (
      <>
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper">
          <div className="flex-grid">
            <div className="flex5">
              <SectionTitle
                category={data.category}
                title={data.title}
                color="blue"
                link="software-consulting"
              />
              <div
                className="body"
                dangerouslySetInnerHTML={{
                  __html: data.htmlNode.childMarkdownRemark.html,
                }}
              />
            </div>
            <div className="flex1 small-image">
              {smallImage && (
                <GatsbyImage
                  image={smallImage}
                  alt="Software section image"
                  className="top-margin-40"
                />
              )}
            </div>
          </div>
          <div className="top-margin-20">
            {[1, 2, 3, 4].map((num) => {
              return (
                <div key={num} className="marketingCard">
                  <MarketingCard service={data[`marketingCard${num}`]} />
                </div>
              )
            })}
          </div>
        </div>
      </>
    )
  } else if (sectionType === 'Design') {
    const designImage = getImage(data.image) || data.image
    return (
      <>
        <BackgroundAngle color="255,255,255" slash="forward" />
        <div className="page-wrapper">
          <div className="flex-grid">
            <div className="flex5">
              <SectionTitle
                category={data.category}
                title={data.title}
                color="blue"
                link="web-design"
              />
              <div
                className="body"
                dangerouslySetInnerHTML={{
                  __html: data.htmlNode.childMarkdownRemark.html,
                }}
              />
            </div>
            <div className="flex1 small-image">
              {designImage && (
                <GatsbyImage
                  image={designImage}
                  alt="Design section image"
                  className="top-margin-40"
                />
              )}
            </div>
          </div>
        </div>
      </>
    )
  } else if (sectionType === 'Customers') {
    return (
      <>
        <div className="page-wrapper">
          <SectionTitle
            category={data.category}
            title={data.title}
            color="white"
            link="local-marketing"
          />
          <div
            className="body text-white"
            dangerouslySetInnerHTML={{
              __html: data.htmlNode.childMarkdownRemark.html,
            }}
          />
          <div className="text-white top-margin-40">
            {[1, 2].map((number, i) => {
              return (
                <div key={i} className="marketingCard">
                  <MarketingCard service={data[`marketingCard${number}`]} />
                </div>
              )
            })}
          </div>
        </div>
      </>
    )
  }
}

export default PageSection
