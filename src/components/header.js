import React from 'react'
import { GatsbyImage, getImage } from 'gatsby-plugin-image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'

const Header = ({ data }) => {
  const image = getImage(data.image) || data.image

  return (
    <header>
      <div
        className={`header-image ${data.fullHeight ? 'full-height' : 'half-height'}`}
      >
        {image && (
          <GatsbyImage
            image={image}
            alt={data.alt || 'Header image'}
            style={{ height: '100%' }}
            imgStyle={{ objectFit: 'cover' }}
          />
        )}
      </div>
      <div
        className={`page-wrapper ${data.fullHeight ? 'full-height' : 'half-height'}`}
      >
        <div className="header-image__title-container">
          {data.fullHeight && <div>Hi, it's time to</div>}
          <div
            className={`title-container__title ${data.fullHeight === false ? 'smaller' : ''}`}
          >
            {data.title}
          </div>
          <div className="divider" />
          <div className="title-container__subTitle">
            {data.subTitle.split('\n').map((item, i) => {
              return <div key={i}>{item}</div>
            })}
          </div>
        </div>
        {data.fullHeight && (
          <div className="header-image__arrow">
            <FontAwesomeIcon icon={faChevronDown} />
          </div>
        )}
      </div>
    </header>
  )
}

export default Header
