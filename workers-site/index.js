import {
  getAssetFromKV,
  mapRequestToAsset,
  serveSinglePageApp,
} from '@cloudflare/kv-asset-handler'

addEventListener('fetch', (event) => {
  try {
    event.respondWith(handleEvent(event))
  } catch (e) {
    event.respondWith(new Response('Internal Error', { status: 500 }))
  }
})

async function handleEvent(event) {
  try {
    let resp = await getAssetFromKV(event, {})

    // SPA用
    // let resp = await getAssetFromKV(event, {
    //   mapRequestToAsset: serveSinglePageApp,
    // });

    resp = new Response(resp.body, {
      status: resp.status,
      headers: resp.headers,
      statusText: resp.statusText,
    })

    resp.headers.set(
      'Content-Security-Policy',
      `script-src 'self' 'unsafe-inline' data: www.google-analytics.com www.google.com www.gstatic.com js.hs-scripts.com js.hscollectedforms.net js.hs-analytics.net js.hs-banner.com static.cloudflareinsights.com pagead2.googlesyndication.com www.googletagmanager.com js-eu1.hs-scripts.com js-eu1.hs-banner.com js-eu1.hs-analytics.net js-eu1.hscollectedforms.net ep2.adtrafficquality.google relvan.com tpc.googlesyndication.com unpkg.com; style-src 'self' 'unsafe-inline' fonts.googleapis.com fonts.gstatic.com; connect-src 'self' 'unsafe-inline' www.google-analytics.com forms.hubspot.com getform.io forms-eu1.hscollectedforms.net ep1.adtrafficquality.google csi.gstatic.com forms.hscollectedforms.net pagead2.googlesyndication.com; img-src 'self' data: www.google-analytics.com www.datocms-assets.com forms.hsforms.com track.hubspot.com forms-eu1.hsforms.com track-eu1.hubspot.com pagead2.googlesyndication.com csi.gstatic.com www.googletagmanager.com cdnjs.cloudflare.com; font-src 'self' data: fonts.gstatic.com`
    )
    resp.headers.set('X-XSS-Protection', '1; mode=block')
    resp.headers.set('X-Frame-Options', 'SAMEORIGIN')
    resp.headers.set('X-Content-Type-Options', 'nosniff')
    resp.headers.set('Referrer-Policy', 'same-origin')
    resp.headers.set(
      'Permissions-Policy',
      'accelerometer=(), camera=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()'
    )
    return resp
  } catch (e) {
    try {
      let notFoundResponse = await getAssetFromKV(event, {
        mapRequestToAsset: (req) =>
          new Request(`${new URL(req.url).origin}/404.html`, req),
      })

      return new Response(notFoundResponse.body, {
        ...notFoundResponse,
        status: 404,
      })
    } catch (e) {}

    return new Response(e.message || e.toString(), { status: 500 })
  }
}
