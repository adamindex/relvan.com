const path = require('path')

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    graphql(`
      {
        allDatoCmsSkill {
          edges {
            node {
              slug
            }
          }
        }
        allDatoCmsPortfolio {
          edges {
            node {
              slug
            }
          }
        }
        allDatoCmsBlogPost {
          edges {
            node {
              slug
            }
          }
        }
        allDatoCmsLandingPage {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      if (result.errors) {
        console.log(result.errors)
        reject(result.errors)
      }

      // Create skill pages
      result.data.allDatoCmsSkill.edges.forEach(({ node: skill }) => {
        console.log('Creating skill page:', skill.slug)
        createPage({
          path: `/${skill.slug}`,
          component: path.resolve(`./src/templates/skill.js`),
          context: {
            slug: skill.slug,
          },
        })
      })

      // Create portfolio pages
      result.data.allDatoCmsPortfolio.edges.forEach(({ node: portfolio }) => {
        console.log('Creating portfolio page:', portfolio.slug)
        createPage({
          path: `/portfolio/${portfolio.slug}`,
          component: path.resolve(`./src/templates/portfolio.js`),
          context: {
            slug: portfolio.slug,
          },
        })
      })

      // Create blog post pages
      result.data.allDatoCmsBlogPost.edges.forEach(({ node: post }) => {
        console.log('Creating blog post page:', post.slug)
        createPage({
          path: `/blog/${post.slug}`,
          component: path.resolve(`./src/templates/blog-post.js`),
          context: {
            slug: post.slug,
          },
        })
      })

      // Create landing pages
      result.data.allDatoCmsLandingPage.edges.forEach(({ node: page }) => {
        console.log('Creating landing page:', page.slug)
        createPage({
          path: `/${page.slug}`,
          component: path.resolve(`./src/templates/landing-page.js`),
          context: {
            slug: page.slug,
          },
        })
      })

      resolve()
    })
  })
}
