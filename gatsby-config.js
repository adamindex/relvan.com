require("dotenv").config();

module.exports = {
  siteMetadata: {
    title: `Marketing`,
    siteUrl: `https://relvan.com`,
  },
  plugins: [
    `gatsby-plugin-sass`,
    // Move this configuration earlier in the plugins array
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-highlight-code`,
            options: {
              terminal: 'carbon',
              theme: 'one-dark',
              lineNumbers: true
            }
          },
        ],
      },
    },
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-plugin-no-sourcemaps`,
    },
    {
      resolve: `gatsby-source-datocms`,
      options: {
        apiToken: process.env.DATO_API_TOKEN,
      },
    },
    {
      resolve: 'gatsby-plugin-google-gtag',
      options: {
        trackingIds: [
          'G-7S9FPWKVYN', // Replace with your Google Analytics Measurement ID
        ],
        pluginConfig: {
          head: true,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        query: `
        {
          allSitePage {
            nodes {
              path
            }
          }
        }
        `,
        resolveSiteUrl: () => 'https://relvan.com', // Replace with your actual site URL
        resolvePages: ({
          allSitePage: { nodes: allPages },
        }) => {
          return allPages.map(page => {
            return { ...page }
          })
        },
        serialize: ({ path }) => {
          return {
            url: path,
            changefreq: 'daily',
            priority: 0.7,
          }
        },
        excludes: ['/404', '/dev-404-page/', '404.html'], // Add your exclude paths here
      },
    },
  ],
  trailingSlash: 'never',
};
