# Relvan Online Marketing

A website that show the skills and services.

![alt text](preview.png "Relvan Online Marketing")

# Running locally

```
// Install dependencies
yarn install

// Run development server on localhost:8000
yarn start

// Build static files
yarn build

// Serve static files on localhost:9000
yarn serve
```

For detailed explanation on how things work, check out [Gatsby.js docs](https://www.gatsbyjs.com/).
